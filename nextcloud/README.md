
## Nexcloud + Collabora + SSL + Nginx reverse + Mysql

I set a Nextcloud deployment by Docker-compose, it a simple a useful statement where you just have to refill your full hostdomain and that is all. 

### images:

- [nextcloud:latest](https://hub.docker.com/_/nextcloud)  
- [jrcs/letsencrypt-nginx-proxy-companion](https://hub.docker.com/r/jrcs/letsencrypt-nginx-proxy-companion)
- [collabora/code](https://hub.docker.com/r/collabora/code)
- [mariadb](https://hub.docker.com/_/mariadb)
- [jwilder/nginx-proxy:alpine](https://hub.docker.com/r/jwilder/nginx-proxy)